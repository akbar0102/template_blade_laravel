@extends('layout.master')
@section('judul')
    Detail Cast
@endsection
@section('content')
<div class="card">
    <div class="card-body">
      <h4 class="card-title">Show Cast dengan id: {{$cast->id}}</h4>
      <p class="card-text">Nama: {{$cast->nama}} <br> Umur: {{$cast->umur}} <br> Bio: {{$cast->bio}}</p>
    </div>
</div>
@endsection